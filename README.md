# Spring Consul Service Starter

## Note this library is still under development and is not ready for production environments

## Overivew

This library provides native integration with Hashicorp Consul for Spring Boot
applications.

## Usage
```xml
<dependency>
    <groupId>org.simtp</groupId>
    <artifactId>spring-consul-service-starter</artifactId>
    <version>0-SNAPSHOT</version>
</dependency>
```

